package xyz.benhigh.kgadwall

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.widget.CheckBox
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_edit_meal.*
import org.jetbrains.anko.activityUiThreadWithContext
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import xyz.benhigh.kgadwall.backend.Item
import xyz.benhigh.kgadwall.backend.Meal
import xyz.benhigh.kgadwall.backend.ShoppingCart
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Activity for creating. editing, and deleting meals in the cart.
 *
 * This activity takes in an integer value via an intent bundle, with the key
 * "mealID". This value can be any number between 0 and Kotlin's maximum
 * integer value, inclusively. If the value is 0, then the activity assumes the
 * user is intending to create a new meal. Otherwise, the activity assumes the
 * user is intending to modify an existing meal, and will search the database
 * for the meal. If the value is not 0, it should point to a GUID of a meal in
 * the shopping cart.
 *
 * It should never occur under normal circumstances, but if the activity is
 * passed a nonzero item ID that does not correspond to an existing meal in the
 * cart, then it will act as if the user is attempting to create a new meal,
 * although the delete button will still appear. Saving the meal under this
 * circumstance will simply create a new meal, while deleting will do nothing.
 *
 * @author Ben High
 */
class EditMealActivity : AppCompatActivity() {
	private val cart = ShoppingCart()
	private var targetMeal = Meal(0, "", 0)
	private var targetId = 0
	private val itemLayoutToItem = HashMap<LinearLayout, Item>()

	/**
	 * Create the activity, get the bundled ID, add the loading screen, and
	 * start loading the data.
	 *
	 * This activity takes in a meal's GUID value via a bundle extra. This
	 * method extracts that GUID, setting the targetId attribute to the GUID,
	 * adds a loading message to the screen, and then calls the asynchronous
	 * method to load items and meals into the cart.
	 *
	 * @param savedInstanceState The save state for the activity.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_edit_meal)

		targetId = intent.extras.getInt("mealID")

		editMealItemsLayout.addView(MessageScreenComponent(this, R.string.cart_loading_message, R.drawable.ic_storage_accent_24dp, false).layoutItem)

		loadData(this)
	}

	/**
	 * Initializes the options menu.
	 *
	 * This method initializes the options menu for the action bar, found in
	 * /res/menu/edit_meal_menu.xml. Additionally, this method will also check
	 * if a meal's GUID has been passed in (which would indicate that the
	 * activity is editing a meal). If it finds no GUID passed in, then the
	 * menu removes the delete option, as deleting an object that doesn't even
	 * exist makes approximately no sense at all.
	 *
	 * @param menu The menu in which to inflate our menu.
	 * @return Always true because we want to show the options menu.
	 */
	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.edit_meal_menu, menu)

		// Remove the delete button if we're working with a new item
		if (targetId == 0) {
			menu!!.removeItem(R.id.editMealDeleteMenuButton)
		}

		return true
	}

	/**
	 * Bind actions to the delete, save, and back menu buttons.
	 *
	 * The delete button makes a call to deleteAndFinish(). The save button
	 * makes a call to saveAndFinish(). The back button is an alias to
	 * Android's system UI back button.
	 *
	 * @param item The selected menu item.
	 * @return Always true because we want to consume the tap action.
	 */
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when {
			item!!.itemId == R.id.editMealDeleteMenuButton -> {
				deleteAndFinish()
			}
			item.itemId == R.id.editMealSaveMenuButton -> {
				saveAndFinish()
			}
			item.itemId == android.R.id.home -> {
				onBackPressed()
			}
		}
		return true
	}

	/**
	 * Checks if there are changes to the meal, and displays a prompt.
	 *
	 * This method, also called when tapping the back button on the menu,
	 * checks if the entered name and selected items differ from the respective
	 * values on the target meal. If so, an alert is displayed, giving the user
	 * the option to save or discard their changes, or simply cancel the exit
	 * event. If no changes are detected, the activity simply finishes.
	 */
	override fun onBackPressed() {
		val selectedItems = getSelectedItems()

		// Only prompt the user to save when the name or selected items differ
		// from the target meal's values.
		if (mealNameInput.text.toString() != targetMeal.name ||
				selectedItems != targetMeal.items) {
			// Show a warning popup, giving the user the ability to save or
			// discard their changes
			val alert = AlertDialog.Builder(this)

			alert.setMessage(R.string.save_discard_message_title)
			alert.setPositiveButton(R.string.save_discard_message_save) { _, _ ->
				saveAndFinish()
			}
			alert.setNegativeButton(R.string.save_discard_message_discard) { _, _ ->
				finish()
			}
			alert.setNeutralButton(android.R.string.cancel) { dialog, _ ->
				dialog.dismiss()
			}
			alert.show()
		} else {
			finish()
		}
	}

	/**
	 * (Asynchronous) Removes a meal and closes the activity.
	 *
	 * This method simply calls the removeMeal method in the shopping cart, and
	 * finishes the activity on the UI thread.
	 */
	private fun deleteAndFinish() = doAsync {
		cart.removeMeal(targetMeal)

		// Finish the activity on the UI thread
		uiThread {
			finish()
		}
	}

	/**
	 * Validate inputs, and then create a new meal or save the existing one.
	 *
	 * This method checks that a name has been entered and that at least one
	 * item has been selected. If both criteria are satisfied, the method then
	 * launches an asynchronous task which either creates a new meal and adds
	 * it to the cart, or modifies the target meal, depending on whether the
	 * target meal ID is 0 or not. If the validation fails, an appropriate
	 * alert is shown to advise the user to check their inputs.
	 */
	private fun saveAndFinish() {
		// Get all selected items
		val selectedItems = getSelectedItems()

		// Validate the inputs. Make sure that the name is not blank, and at
		// least one item is selected
		when {
			mealNameInput.text.toString().isBlank() -> {
				val errorAlert = AlertDialog.Builder(this)

				errorAlert.setTitle(R.string.edit_meal_error_title)
				errorAlert.setMessage(R.string.edit_meal_empty_fields_error)
				errorAlert.setPositiveButton(android.R.string.ok) { dialog, _ ->
					dialog.dismiss()
				}
				errorAlert.show()
			}
			selectedItems.isEmpty() -> {
				val errorAlert = AlertDialog.Builder(this)

				errorAlert.setTitle(R.string.edit_meal_error_title)
				errorAlert.setMessage(R.string.edit_meal_no_items_error)
				errorAlert.setPositiveButton(android.R.string.ok) { dialog, _ ->
					dialog.dismiss()
				}
				errorAlert.show()
			}
			else -> doAsync {
				// Create a new meal if the ID of the target meal is 0. Otherwise,
				// modify the existing meal
				if (targetMeal.id == 0) {
					val newMeal = Meal(generateRandomUniqueID(),
							mealNameInput.text.toString(),
							System.currentTimeMillis() / 1000)
					newMeal.items = selectedItems

					cart.addMeal(newMeal)
				} else {
					// Set name and last edit time
					targetMeal.name = mealNameInput.text.toString()
					targetMeal.lastEdit = System.currentTimeMillis() / 1000

					// Add all selected items that aren't already in the meal
					for (item in selectedItems) {
						if (!targetMeal.items.contains(item)) {
							targetMeal.items.add(item)
						}
					}

					// Get a list of items to remove from the meal
					val toRemove = ArrayList<Item>()
					for (item in targetMeal.items) {
						if (!selectedItems.contains(item)) {
							toRemove.add(item)
						}
					}

					// Remove the items from the meal. We have to do it here as
					// opposed to the previous for-loop to avoid crashing with
					// a ConcurrentModificationException.
					for (item in toRemove) {
						targetMeal.items.remove(item)
					}

					cart.saveMeals()
				}

				// Finish the activity on the UI thread
				uiThread {
					finish()
				}
			}
		}
	}

	/**
	 * (Asynchronous) Load items and meals from the database, assign the target
	 * meal, and populate the screen.
	 *
	 * This method initializes the database for future use, and then loads both
	 * the items and meals into the cart, since items are required for meals to
	 * work. Once these are loaded in, the method attempts to identify the meal
	 * with the passed in target ID, if the ID is nonzero. If the target meal
	 * cannot be found, then the blank one is used. The saveAndFinish and
	 * deleteAndFinish methods know how to handle such events. Once the item is
	 * found, the UI thread is invoked with the application context to set the
	 * default value in the EditText, as well insert item components into the
	 * activity's ScrollView.
	 *
	 * @param context The application context, for database initialization.
	 */
	private fun loadData(context: Context) = doAsync {
		cart.initializeDB(context)
		cart.loadItems()
		cart.loadMeals()

		// IDs can never be greater than 0, so if that's passed in, our target
		// meal is blank, so do nothing. Otherwise, get the target meal from
		// the cart. If the meal cannot be identified, then the blank meal will
		// be used
		if (targetId > 0) {
			for (meal in cart.meals) {
				if (meal.id == targetId) {
					targetMeal = meal
					break
				}
			}
		}

		// Populate the screen on the UI thread, with the application context
		activityUiThreadWithContext {
			mealNameInput.setText(targetMeal.name)

			editMealItemsLayout.removeAllViews()
			for (item in cart.items) {
				val component = ShoppingListItemComponent(this, item)
				if (item in targetMeal.items) {
					component.checkBox.isChecked = true
				}

				// Use a HashMap to keep track of which items belong to which
				// layout items
				itemLayoutToItem[component.layoutItem] = item
				editMealItemsLayout.addView(component.layoutItem)
			}
		}
	}

	/**
	 * Gets a list of all selected items.
	 *
	 * This method uses the HashMap to identify which items are associated with
	 * the item components whose checkboxes have been checked.
	 *
	 * @return An ArrayList of items whose components have been checked.
	 */
	private fun getSelectedItems(): ArrayList<Item> {
		val selectedItems = ArrayList<Item>()
		for (i in 0 until editMealItemsLayout.childCount) {
			val component = editMealItemsLayout.getChildAt(i)
			val item = itemLayoutToItem[component]!!
			if (component.findViewWithTag<CheckBox>("checkbox").isChecked) {
				selectedItems.add(item)
			}
		}
		return selectedItems
	}

	/**
	 * Generate a random GUID value.
	 *
	 * This method creates a list of existing GUID values among meals in the
	 * cart, and then generates a new value between 1 and Kotlin's maximum
	 * integer value. If the newly created value already exists in the cart,
	 * the method generates a new one. This process repeats until a unique
	 * ID number has been generated, which is then returned.
	 *
	 * @return A unique numerical identifier value.
	 */
	private fun generateRandomUniqueID(): Int {
		// Create a list of existing IDs
		val existingIDs = ArrayList<Int>()
		for (meal in cart.meals) {
			existingIDs.add(meal.id)
		}

		// Generate a new ID until a unique value is found
		var newID: Int
		val random = Random(System.currentTimeMillis())
		do {
			newID = random.nextInt(Int.MAX_VALUE - 1) + 1
		} while (existingIDs.contains(newID))

		return newID
	}
}