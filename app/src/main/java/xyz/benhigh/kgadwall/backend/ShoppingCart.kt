package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Room
import android.content.Context

/**
 * Shopping cart class to hold items and meals.
 *
 * This class contains all items and meals, and provides various methods to
 * add, remove, load, save, and sort data.
 *
 * @author Ben High
 */
class ShoppingCart {
	private var database: KGadwallDatabase? = null
	val items = ArrayList<Item>()
	val meals = ArrayList<Meal>()

	/**
	 * Initialize the database for future use.
	 *
	 * This method builds and initializes the Room database so it can be used.
	 *
	 * @param context The application context.
	 */
	fun initializeDB(context: Context) {
		if (database == null) {
			database = Room.databaseBuilder(context, KGadwallDatabase::class.java, "kgadwall").build()
		}
	}

	/**
	 * Load all items into the cart.
	 *
	 * This method handles loading and parsing information from the items
	 * table in the database, populating the items ArrayList.
	 */
	fun loadItems() {
		items.clear()
		val dbItems = database!!.itemDao().getAll()
		for (item in dbItems) {
			items.add(item)
		}
	}

	/**
	 * Load all meals and relations into the cart.
	 *
	 * This method handles loading and parsing information from the meals
	 * table in the database, populating the meals ArrayList. Additionally,
	 * this method loads in the relationship models between items and meals.
	 */
	fun loadMeals() {
		meals.clear()
		val dbMeals = database!!.mealDao().getAll()

		// Load item-meal relationships
		for (meal in dbMeals) {
			val relations = database!!.itemMealRelationDao().getForMealID(meal.id)

			for (relation in relations) {
				// Loop through the items database so we have a proper full
				// link to each item object, instead of just getting a new copy
				// from the database
				for (item in items) {
					if (item.id == relation.itemID) {
						meal.items.add(item)
						break
					}
				}
			}

			meals.add(meal)
		}
	}

	/**
	 * Save all items.
	 *
	 * This method cycles through all items in the cart and makes a call to the
	 * item DAO to insert all items. Because of how the item DAO is set up, any
	 * items that already exist in the database will be overwritten, whereas
	 * any other items will be inserted normally.
	 */
	fun saveItems() {
		for (item in items) {
			database!!.itemDao().insert(item)
		}
	}

	/**
	 * Save all meals and reestablish relationships.
	 *
	 * This method first clears the relationship table, then cycles through all
	 * meals, inserting each meal into the meals table. The method then creates
	 * all relations between meals and their child items, and adds those to the
	 * relationship table.
	 */
	fun saveMeals() {
		database!!.itemMealRelationDao().emptyTable()

		for (meal in meals) {
			database!!.mealDao().insert(meal)

			val relations = meal.prepareRelations()
			for (relation in relations) {
				database!!.itemMealRelationDao().insert(relation)
			}
		}
	}

	/**
	 * Add an item to the cart, and then save the items.
	 *
	 * This method simply takes in an item, adds it to the ArrayList, and then
	 * optionally saves the items database. The save feature can be disabled by
	 * setting the doSave argument to false.
	 *
	 * @param item The item to be added to the cart.
	 * @param doSave A boolean to decide whether or not to save the items.
	 */
	fun addItem(item: Item, doSave: Boolean = true) {
		items.add(item)

		if (doSave) {
			saveItems()
		}
	}

	/**
	 * Add a meal to the cart, and then save the meals.
	 *
	 * This method simply takes in an meal, adds it to the ArrayList, and then
	 * optionally saves the meals database. The save feature can be disabled by
	 * setting the doSave argument to false.
	 *
	 * @param meal The meal to be added to the cart.
	 * @param doSave A boolean to decide whether or not to save the meals.
	 */
	fun addMeal(meal: Meal, doSave: Boolean = true) {
		meals.add(meal)

		if (doSave) {
			saveMeals()
		}
	}

	/**
	 * Remove a meal and all of its relations from the database.
	 *
	 * This method takes in a meal item, removing the meal from both the
	 * ArrayList and the database, as well as removing all item-meal relations
	 * associated with this meal from the database.
	 *
	 * @param meal The meal object to be removed.
	 */
	fun removeMeal(meal: Meal) {
		if (meals.contains(meal)) {
			meals.remove(meal)
			database!!.itemMealRelationDao().removeRelationsForMeal(meal.id)
			database!!.mealDao().deleteMealByID(meal.id)
		}
	}

	/**
	 * Remove an item and all of its relations from the database.
	 *
	 * This method takes in an item object, loops through the meals list,
	 * removing all references to the item in all meals, then removes all
	 * relations associated with the object in the database, as well as the
	 * item itself from the database.
	 *
	 * @param item The item object to be removed.
	 */
	fun removeItem(item: Item) {
		if (items.contains(item)) {
			// Load the meals from the database if it hasn't already been done
			if (meals.isEmpty()) {
				loadMeals()
			}

			// Remove links from this item to any meals
			for (meal in meals) {
				if (meal.items.contains(item)) {
					meal.items.remove(item)
				}
			}

			// Remove the item and all relations from the database
			items.remove(item)
			database!!.itemMealRelationDao().removeRelationsForItem(item.id)
			database!!.itemDao().deleteItemByID(item.id)
		}
	}

	/**
	 * Sort items based on aisle name.
	 *
	 * @param reversed True to reverse the order, false to keep default order.
	 */
	fun sortItems(reversed: Boolean = false) {
		items.sort()

		if (reversed) {
			items.reverse()
		}
	}

	/**
	 * Sort items based on name.
	 *
	 * @param reversed True to reverse the order, false to keep default order.
	 */
	fun sortMeals(reversed: Boolean = false) {
		meals.sort()

		if (reversed) {
			meals.reverse()
		}
	}
}