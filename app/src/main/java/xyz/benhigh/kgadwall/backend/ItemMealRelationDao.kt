package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

/**
 * Data Access Object interface for item-meal relation objects.
 *
 * This interface defines SQL queries for the relations table in the database.
 * This interface provides methods for selecting, inserting, and emptying the
 * table, as well as getting relations for a specific meal and removing
 * relations for a specific meal or item.
 *
 * @author Ben High
 */
@Dao
interface ItemMealRelationDao {
	/**
	 * Method to return a list of all relations in the table.
	 *
	 * @return A list of all relations in the table.
	 */
	@Query("SELECT * FROM ItemToMeal")
	fun getAll(): List<ItemMealRelation>

	/**
	 * Method to insert a relation into the table.
	 *
	 * If a relation with an existing ID is passed in, the entry in the
	 * database is updated instead.
	 *
	 * @param relation The relation object to be inserted or updated.
	 */
	@Insert(onConflict = REPLACE)
	fun insert(relation: ItemMealRelation)

	/**
	 * Method to clear the relations table of all entries.
	 */
	@Query("DELETE FROM ItemToMeal")
	fun emptyTable()

	/**
	 * Method to return a list of all relations in the table whose meal ID
	 * matches the passed in GUID
	 *
	 * @param id The meal GUID to search for.
	 * @return A list of relations relevant to the meal with the passed GUID.
	 */
	@Query("SELECT * FROM ItemToMeal WHERE mealID=:id")
	fun getForMealID(id: Int): List<ItemMealRelation>

	/**
	 * Method to remove all relations related to a passed in item GUID.
	 */
	@Query("DELETE FROM ItemToMeal WHERE itemID=:id")
	fun removeRelationsForItem(id: Int)

	/**
	 * Method to remove all relations related to a passed in meal GUID.
	 */
	@Query("DELETE FROM ItemToMeal WHERE mealID=:id")
	fun removeRelationsForMeal(id: Int)
}