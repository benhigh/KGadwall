package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Data class to hold item information.
 *
 * This class holds an ID, name, aisle, last modification time, and a boolean
 * to dictate whether or not an item is in the cart. This is also an entity,
 * which is generated when loaded from the database. This class also provides a
 * method to alphabetically compare its aisle value with that of another item
 * object, allowing items to be sorted.
 *
 * @author Ben High
 */
@Entity(tableName = "Items")
data class Item(@PrimaryKey val id: Int,
                            var name: String,
                            var aisle: String,
                            var inCart: Boolean,
                            var lastEdit: Long): Comparable<Item> {

	/**
	 * Alphabetically compare this item's aisle value with another item's.
	 *
	 * This method allows item objects to be sorted by aisle name. It just uses
	 * the standard string compareTo method to decide relative order to another
	 * item.
	 *
	 * @param other An item object to compare aisle name against.
	 * @return This item's relative order to the other.
	 */
	override fun compareTo(other: Item): Int {
		return aisle.compareTo(other.aisle)
	}
}