package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Data class to hold meal information.
 *
 * This class holds an ID, name, and last modification time. This is also an
 * entity, which is generated when loaded from the database. This class also
 * provides a method to generate relevant ItemMealRelation objects, and a
 * method to alphabetically compare its aisle value with that of another meal
 * object, allowing meals to be sorted.
 *
 * @author Ben High
 */
@Entity(tableName = "Meals")
data class Meal(@PrimaryKey val id: Int,
                            var name: String,
                            var lastEdit: Long): Comparable<Meal> {
	@Ignore
	var items = ArrayList<Item>()

	/**
	 * Create a list of relations relevant to this meal.
	 *
	 * This method cycles through the items held in this meal, creating a list
	 * of ItemMealRelation objects connecting this meal to all of its child
	 * items.
	 *
	 * @return An ArrayList of ItemMealRelation objects relevant to this meal.
	 */
	fun prepareRelations(): ArrayList<ItemMealRelation> {
		val relations = ArrayList<ItemMealRelation>()

		for (item in items) {
			relations.add(ItemMealRelation(item.id, id))
		}

		return relations
	}

	/**
	 * Alphabetically compare this meal's name value with another meal's.
	 *
	 * This method allows meal objects to be sorted by name. It just uses the
	 * standard string compareTo method to decide relative order to another
	 * meal.
	 *
	 * @param other A meal object to compare aisle name against.
	 * @return This meal's relative order to the other.
	 */
	override fun compareTo(other: Meal): Int {
		return name.compareTo(other.name)
	}
}