package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

/**
 * Data Access Object interface for meal objects.
 *
 * This interface defines SQL queries for the meals table in the database. This
 * interface provides methods for selecting, inserting, and deleting meals from
 * the table.
 *
 * @author Ben High
 */
@Dao
interface MealDao {
	/**
	 * Method to return a list of all meals in the table.
	 *
	 * @return A list of all meals in the table.
	 */
	@Query("SELECT * FROM Meals")
	fun getAll(): List<Meal>

	/**
	 * Method to insert a meal into the table.
	 *
	 * If a meal with an existing GUID is passed in, the entry in the database
	 * is updated instead.
	 *
	 * @param meal The meal object to be inserted or updated.
	 */
	@Insert(onConflict = REPLACE)
	fun insert(meal: Meal)

	/**
	 * Method to clear the meals table of all entries.
	 */
	@Query("DELETE FROM Meals")
	fun emptyTable()

	/**
	 * Method to delete a meal from the table by ID.
	 *
	 * @param id The GUID value of the meal to be deleted.
	 */
	@Query("DELETE FROM Meals WHERE id=:id")
	fun deleteMealByID(id: Int)
}