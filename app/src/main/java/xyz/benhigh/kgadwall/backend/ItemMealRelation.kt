package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Data class to link meals and items together.
 *
 * This class simply stores an item's GUID and a meal's GUID, in order to
 * create a relationship between an item and meal when stored in the database.
 *
 * @author Ben High
 */
@Entity(tableName = "ItemToMeal")
data class ItemMealRelation(@ForeignKey(entity = Item::class,
										parentColumns = ["id"],
										childColumns = ["itemID"])
                                            val itemID: Int,
                            @ForeignKey(entity = Meal::class,
		                            parentColumns = ["id"],
		                            childColumns = ["mealID"])
                                            val mealID: Int) {
	@PrimaryKey(autoGenerate = true) var relID = 0
}